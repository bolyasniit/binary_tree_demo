package com.kenfogel.binarytree;

import com.kenfogel.binarytree.implementation.BinaryTree;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Based on the code found at http://cslibrary.stanford.edu/110/BinaryTrees.html
 *
 * @author Ken Fogel
 */
public class BinaryTreeApp {

    private BinaryTree<String> tree;

    /**
     * Run the various methods in a tree to test it.
     */
    public void perform() {
        tree = new BinaryTree<>();
        String[] data = {"Cat", "dog", "Mous", "duck", "go", "car", "Gene", "more", "Allo"};
        buildATree(data);
        System.out.println("Look up 7 = " + tree.lookup("Cat"));
        System.out.println("Look up 10 = " + tree.lookup("go"));
        System.out.println("Size of tree = " + tree.size());
        System.out.println("Max depth = " + tree.maxDepth());
        System.out.println("Min value = " + tree.minValue());
        System.out.print("Inorder = ");
        tree.printInorderTree();
        System.out.print("Postorder = ");
        tree.printPostorder();
        System.out.println("The Paths");
        for (Comparable<?>[] arrays: hope()) {
            for (Comparable<?> s: arrays) {
                if (s == null) {
                    System.out.print(" ");
                } else {
                System.out.print(s + " ");
                }
            }
            System.out.println();
        }
        

    }

    
    public <T extends Comparable<?>> T[][] hope() {
        System.out.println(tree.maxDepth());
        T[][] array = (T[][])new String[(int)Math.pow(2, tree.maxDepth())][31];
        List<String[]> data = tree.printPaths();
        T root = getRoot(data);
        int currentLineSize = 0;
        int x = 0;
        int level = 1;
        int width = 15;
        int y = 0;
        array[0][width] = root;
        for (String[] dataArray: data) {
            for (int i = 0; i < dataArray.length; i++) {
                currentLineSize += (8/level);
                if (dataArray[i].compareTo((String)root) < 0) {
                    addPaddingLeft(array, level, width, x);
                    width -= (7/level);
                    array[currentLineSize][width] = (T)dataArray[i]; 
                    root = (T)dataArray[i];
                    level++;
                } else if (dataArray[i].compareTo((String)root) > 0) {
                    addPaddingRight(array,level, width, x);
                    width += (7/level);
                    
                    array[currentLineSize][width] = (T)dataArray[i];
                    root = (T)dataArray[i];
                    level++;
                }
                x = currentLineSize;
            }
            level = 1;
            root = getRoot(data);
            width = 15;
            currentLineSize = 0;
            x=0;
        }
        return array;
    }
    
    private <T extends Comparable<?>> void addPaddingLeft(T[][] array, int level, int col, int row) {
        int currentRow = row;
        int currentCol = col;
        for (int i = 0; i < 8/level; i++) {
            System.out.println(currentRow + " row " + currentCol);
            currentRow--;
            currentCol++;
            array[currentRow][currentCol] = (T)"/";
        }
    }
    
    private <T extends Comparable<?>> void addPaddingRight(T[][] array, int level, int col, int row) {
        int currentRow = row;
        int currentCol = col;
        for (int i = 0; i < 8/level; i++) {
            System.out.println(currentRow + " row2 " + currentCol);
            currentRow++;
            currentCol++;
            array[currentRow][currentCol] = (T) "\\";
        }
    }

    private <T extends Comparable<?>> T getRoot(List<String[]> data) {
        String temp = data.get(0)[0];
        for (String[] array : data) {
            if (!(array[0].equals(temp))) {
                return (T)"Error";
            }
        }
        return (T)temp;
    }


    /**
     * Build a tree by inserting the members of the array into the tree.
     *
     * @param data
     */
    public void buildATree(String data[]) {
        for (String number : data) {
            tree.insert(number);
        }
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        BinaryTreeApp bta = new BinaryTreeApp();
        bta.perform();
        System.exit(0);
    }
}
