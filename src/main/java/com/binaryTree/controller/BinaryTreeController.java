/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binaryTree.controller;

import com.kenfogel.binarytree.BinaryTreeApp;
import com.kenfogel.binarytree.implementation.BinaryTree;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.application.Platform;
import javafx.scene.layout.GridPane;

/**
 *
 * @author 1633867
 */
public class BinaryTreeController {
    
    private BinaryTree<String> tree;
    
    @FXML
    public void initialize() {
        tree = new BinaryTree<>();
        String[] data = {"Cat", "dog", "Mous", "duck", "go", "car", "Gene", "more", "stff", "test"};
        buildTree(data);
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(BinaryTreeApp.class.getResource("/root.fxml"));
    }
    
    private void buildTree(String... data) {
        for(String t: data) {
            tree.insert(t);
        }
    }
    
    private void loadTree() {
        GridPane grid = new GridPane();
        
    }
    
}
